-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 26, 2014 at 05:46 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id_message` int(11) NOT NULL AUTO_INCREMENT,
  `message` text,
  `from` int(11) NOT NULL,
  `insert_datetime` datetime DEFAULT NULL,
  `to` int(11) NOT NULL,
  `is_read` tinyint(4) DEFAULT '0',
  `id_client_side` varchar(100) NOT NULL,
  PRIMARY KEY (`id_message`),
  KEY `fk_messages_users_idx` (`from`),
  KEY `fk_messages_users1_idx` (`to`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id_message`, `message`, `from`, `insert_datetime`, `to`, `is_read`, `id_client_side`) VALUES
(8, 'test', 2, '2014-05-25 00:09:43', 1, 1, '20140525050943_user1'),
(9, 'test', 2, '2014-05-25 00:24:47', 1, 1, '20140525052447_user1'),
(10, 'test', 3, '2014-05-25 00:24:58', 1, 1, '20140525052458_user2'),
(11, 'test', 3, '2014-05-25 00:25:05', 1, 1, '20140525052505_user2'),
(15, 'test', 1, '2014-05-25 00:49:15', 3, 0, '20140525054915_user2'),
(16, 'abc', 1, '2014-05-25 00:51:23', 2, 0, '20140525055123_user1'),
(17, 'test', 3, '2014-05-25 00:51:28', 1, 1, '20140525055128_user2'),
(18, 'tstest', 3, '2014-05-25 00:51:31', 1, 1, '20140525055130_user2'),
(19, 'ds', 1, '2014-05-25 00:51:36', 2, 0, '20140525055136_user1'),
(20, 'tstest', 2, '2014-05-25 00:51:40', 1, 1, '20140525055139_user1'),
(21, 'sd', 1, '2014-05-25 00:51:59', 3, 0, '20140525055159_user2'),
(22, 'sdf', 3, '2014-05-25 00:52:10', 1, 1, '20140525055210_user2'),
(23, 'sdfd', 2, '2014-05-25 00:52:12', 1, 1, '20140525055212_user1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `full_name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id_user`, `name`, `full_name`, `email`, `password`) VALUES
(1, 'admin', 'admin', 'admin@test.com', '21232f297a57a5a743894a0e4a801fc3'),
(2, 'user1', 'silk', 'silk@gmail.com', '5339078d0c2b1aaefe831b84a71c64dd'),
(3, 'user2', 'sun', 'sun@gmail.com', 'ebd556e6dfc99dbed29675ce1c6c68e5');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_messages_users` FOREIGN KEY (`from`) REFERENCES `users` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_messages_users1` FOREIGN KEY (`to`) REFERENCES `users` (`id_user`) ON DELETE NO ACTION ON UPDATE NO ACTION;
