function C2X(){

/* private */	



	formatClientTime=function(clientTime){
		result = clientTime.substring(10,12) + ":" + clientTime.substring(12,14)  ;
		return result;
	}
		
	createAdminMessage=function(clientName, clientTime, clientMessage, clientSideId){
		var tab = 	'<div class="msg-admin" id="'+clientSideId+'">'+
					'	<div class="itemdiv dialogdiv">'+
					'		<div class="user"><span class="avatar">Admin</span></div>'+
					'			<div class="body">'+
					'				<div class="name"><a href="#">Admin</a></div><div class="time"><i class="icon-time"></i><span class="blue">'+formatClientTime(clientTime)+'</span></div>'+
					'				<div class="text">' + clientMessage + '</div>'+
					'			</div>'+
					'		</div>'+
					'	</div>'+
					'</div>';
		
					
		$("#"+clientName+"-tab").append(tab);
	};
	
	
	
	createClientMessage=function(clientName, clientTime, clientMessage, isread, clientsideid){
		var li = $("#"+clientName+"-li");
		var badge = $("#"+clientName+"-badge");
		
		var classStatusIsNotRead="";
		
		if(li.hasClass("active")===false && isread==="0"){
			var messageCounter = $(badge).html();
			messageCounter = new Number(messageCounter) + 1;
			$(badge).html(messageCounter);
			$(badge).show();	
			classStatusIsNotRead = "not-read";
		}
		else{
			$(badge).html("");
			$(badge).hide();
		}
		
		
		var tab = 	'<div class="itemdiv dialogdiv '+classStatusIsNotRead+'" id="'+clientsideid+'">'+
					'	<div class="user">'+
					'		<span class="avatar">Guest</span>'+
					'	</div>'+
					'	<div class="body">'+
					'		<div class="time">'+
					'			<i class="icon-time"></i><span class="blue">' + formatClientTime(clientTime) + ' </span>'+
					'		</div>'+
					'		<div class="name">'+
					'			<a href="#">' + clientName + '</a>'+
					'		</div>'+
					'		<div class="text">' + clientMessage + '</div>'+
					'	</div>'+
					'</div>';
					
		$("#"+clientName+"-tab").append(tab);
		
		
	}
	
	createNewTab=function(name, clientFrom){
		$("div.tabs-left ul.nav").append("<li id='"+name+"-li' data-id='"+clientFrom+"'><a href='#"+name+"-tab' data-toggle='tab'><span class='badge pull-right' style='display:none;' id='"+name+"-badge'>0</span> &nbsp;&nbsp;"+name+"</a> </li>");
		$("div.tabs-left div.tab-content").append("<div class='tab-pane' id='"+name+"-tab'></div>");
		
		$('a[data-toggle="tab"]').off('shown.bs.tab');	
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			var id = $(this).attr('href');		
			var badgeId=id.replace("-tab", "-badge");	
			$(""+badgeId).html("0").hide();
			
			//update status is_read		
			var ids = "0";
			
			$(""+id).find(".not-read").each(function(){
				ids +=","+$(this).attr('id');
			});
			
			if(ids !== "0"){			
				$.ajax({
					url: baseUrl + 'msgsrv/update',
					type: 'POST',
					async: true,
					// cache: false,
					timeout: 50000,
					data:{"ids":ids},
					dataType: 'JSON',
				}).done(function(data) {
				
					if(data.status=="1"){
						$(""+id).find(".not-read").each(function(){
						$(this).removeClass('not-read');
					});
					}else{
						console.log(data.message + " " + new Date());
					}						
				}).fail(function(XMLHttpRequest, textStatus, errorThrown) {
					//parseMessage("error", textStatus + " (" + errorThrown + ")");
					//setTimeout(poolingMessage, 5000);
					// console.log(textStatus);
				}).always(function() {});
				
			}
		});
	}

	renderLayout=function(data)
	{			
		if(data.length>0){	
			
			$.each(data, function(i, obj) {
				var arrClientID = obj.id_client_side.split("_");
				var clientTime = arrClientID[0];
				var clientName= arrClientID[1];
				var clientTo = obj.to;
				var clientFrom = obj.from;
				var isread = obj.is_read;
				var message = obj.message;
				var clientSideId = obj.id_client_side;				
				
				if(clientTo==="1"){ 
					//create tab?					
					if($("#"+clientName+"-tab").size() == 0){	
						createNewTab(clientName, clientFrom);
					}
					//just adding the client message to existing gab
					if($("#"+clientSideId).size() == 0){
						createClientMessage(clientName, clientTime, message, isread, clientSideId);
					}
				}else{					
					//just adding the admin message to existing tab	
					if($("#"+clientSideId).size() == 0){
						createAdminMessage(clientName, clientTime, message, clientSideId);	
					}			
				}
			});
		}
	}
	
	function poolingMessage(){
		$.ajax({
			type:"GET",
			url: baseUrl + 'msgsrv/unread',
			//cache :false,
			async: true,
			timeout: 50000,
			dataType: 'JSON',
			success : function(data) {
				renderLayout(data.message);
				
				setTimeout(poolingMessage, 5000);
			}
		});
	}
	
	/* public */
	this.initPage=function(){
		$.ajax({
			type:"GET",
			url: baseUrl + 'msgsrv/',
			//cache :false,
			async: true,
			timeout: 50000,
			dataType: 'JSON',
			success : function(data) {
				renderLayout(data.message);
				
				setTimeout(poolingMessage, 5000);
			}
		});
	}
}


$(function() {
	var objC2X = new C2X();
	objC2X.initPage();
});

