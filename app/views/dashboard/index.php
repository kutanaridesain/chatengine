	<!-- <h1><?php echo $title; ?></h1>
	<a href="<?php echo $GLOBALS['conf']['SITE']['baseURL']; ?>user/logout">Logout</a> -->
	<script>
	 var baseUrl = "<?php echo $GLOBALS['conf']['SITE']['baseURL'];  ?>";
	</script>
	<!-- TODO: REMOVE STYLE -->
	<style>
	.chat{
		position:fixed;
		bottom:0px;
		width:250px;
		height:100px;
		background-color:gray;
	}

	#left-chat{
		left:0px;
	}
	
	#admin-chat{
		left:45%;
		background-color:#bababa;
	}

	#right-chat{
		right:0px;
	}
	</style>

	<div class="page-header">
	  <h4>Dashboard Chating</h4>
	</div>
	<div class="panel">
		<div class="tabbable tabs-left clearfix">
			<ul class="nav nav-tabs nav-stacked">
				<li class="active"><a href="#home-tab" data-toggle="tab">Home</a></li>
				<!--<li><a href="#profile-tab" data-toggle="tab">Profile</a></li>
				<li><a href="#extra-tab" data-toggle="tab">Extra</a></li>-->
			 </ul>
		
			<div class="tab-content">
				<div class="tab-pane active" id="home-tab">
					Welcome to the Jungle.
				</div>
				<!--<div class="tab-pane" id="profile-tab">
					Profile
				</div>
				<div class="tab-pane" id="extra-tab">
					Extra
				</div>-->
			</div>
		</div>
	</div>
  
	<div id="left-chat" class="chat">
		<input type="text" name="left-text" id="lefttext" value=""  data-id="2" data-name="user1"/>
		<input type="button" name="left-button" id="leftbutton" value="Send" class="client-button"/>
		<h3>USER 1</h3>
	</div>
	
	
	<div id="admin-chat" class="chat">
		<input type="text" name="admin-text" id="admintext" value="" data-id="1" data-name="admin"/>
		<input type="button" name="admin-button" id="adminbutton" value="Send" />
		<h2>ADMIN</h2>
	</div>
	
  
	<div id="right-chat" class="chat">
		<input type="text" name="right-text" id="righttext" value="" data-id="3" data-name="user2"/>
		<input type="button" name="right-button" id="rightbutton" value="Send" class="client-button"/>
		<h3>USER 2</h3>
	</div>
	<script type="text/javascript">
		var timeText = "";
		
		function getCurrentDate(){
			var dt = new Date();
			var month = dt.getMonth()+1;
			if(month < 10){	month = "0" + month; }
			
			var day = dt.getDate();
			if(day < 10){ day = "0" + day; }
			
			var hour = dt.getHours();
			if(hour < 10){ hour = "0" + hour; }
			
			var min = dt.getMinutes();
			if(min < 10){ min = "0" + min; }
			
			var sec = dt.getSeconds();
			if(sec < 10){ sec = "0" + sec; }
			
			var strDt = dt.getFullYear() + month + day + hour + min + sec;
			timeText = hour + ":" + min + ":" + sec;
			return strDt;
		}
		
		function sendMessage(obj){
			var textBox = $(obj).prev();
			var message = $(textBox).val();
			var from = $(textBox).data("id");
			var name = $(textBox).data("name");	
			var clientid = getCurrentDate() + "_" + name;
			
			$(textBox).val("").focus();
			
			$.ajax({
				url: baseUrl + 'msgsrv/insert',
				type: 'POST',
				async: true,
				data: {"message":message, "from":from, to:"1", "clientid":clientid },
				timeout: 50000,
				dataType: 'JSON'
			}).done(function(data) {
				
				//console.log(data);
			}).fail(function(XMLHttpRequest, textStatus, errorThrown) {
				//parseMessage("error", textStatus + " (" + errorThrown + ")");				
				// console.log(textStatus);
			});
		};
		
	
		$(document).ready(function() {
			$("input[type='button'].client-button").click(function(){
				sendMessage($(this));
			});
			
			$("#adminbutton").click(function(){
				var activeLi = $(".nav-stacked li.active");
				var activeLiId = $(activeLi).attr("id");
				var activeTabId = activeLiId.replace("-li", "");
				var activeTabId = activeTabId.replace("#", "");
				var obj = $(this);
				var to = $(activeLi).data("id");
				
				if(to != undefined){				
					var textBox = $(obj).prev();
					var message = $(textBox).val();
					var from = $(textBox).data("id");
					var name = $(textBox).data("name");	
					var clientid = getCurrentDate() + "_" + activeTabId;
					
					$(textBox).val("").focus();
					
					$.ajax({
						url: baseUrl + 'msgsrv/insert',
						type: 'POST',
						async: true,
						data: {"message":message, "from":from, to: to, "clientid":clientid },
						timeout: 50000,
						dataType: 'JSON'
					}).done(function(data) {
						
						//console.log(data);
					}).fail(function(XMLHttpRequest, textStatus, errorThrown) {
						//parseMessage("error", textStatus + " (" + errorThrown + ")");				
						// console.log(textStatus);
					});
				
				}
			})		
		});
		
		
		
	</script>
