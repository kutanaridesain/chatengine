<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>ChatEngine - <?php echo $title; ?></title>
    <!-- Sets initial viewport load and disables zooming  -->
    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no">
    <link rel="shortcut icon" href="favicon_16.ico"/>
    <link rel="bookmark" href="favicon_16.ico"/>
    <!-- site css -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/bootflat.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,800,700,400italic,600italic,700italic,800italic,300italic" rel="stylesheet" type="text/css">
    <script src="assets/js/jquery-1.11.0.min.js" type="text/javascript" charset="utf-8"></script>
  <script src="assets/js/jquery.slimscroll.min.js" type="text/javascript" charset="utf-8"></script>
  <script src="assets/js/app.js" type="text/javascript" charset="utf-8"></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>
<body>
<nav class="navbar-default navbar-static-top nav-shadow" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <span class="navbar-brand">2xChatEngine <small class="label label-info">v.1.0</small></span>
    </div>
    <div class="nav-header pull-right">Logout</div>
    <!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="row-fluid text-center">
    <ul class="menu" id="mainMenu">
      <li><a href="dashboard">Dashboard</a></li>
      <li><a href="designer">Client Designer</a></li>
      <li><a href="configuration">Configuration</a></li>
    </ul>
</div>
<!-- container -->
<div class="bg"></div>  
</div>
<div class="container">
  <?php echo $content; ?>
</div>
<!-- /.container -->
<!--footer-->
  <div class="site-footer">
    <div class="container">
      
      <hr class="soft-line" />
      <div class="copyright clearfix">
        <p><b>ChatEngine</b>&nbsp;&nbsp;&nbsp;&nbsp;<a href="ce/about">About</a>&nbsp;&bull;&nbsp;<a href="ce/help">Help</a></p>
        <p>&copy; 2014 <a href="http://www.2xStudio.com" target="_blank">2xStudio</a>. All rights reserved. &nbsp;&nbsp;Code licensed under <a href="http://opensource.org/licenses/gpl.html" target="_blank" rel="external nofollow">GPL License</a>.</p>
      </div>
    </div>
  </div>
  <script src="assets/js/modernizr-2.5.3-min.js" type="text/javascript" charset="utf-8"></script>
  <script src="assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript" charset="utf-8">
    var controller = '<?php echo $controller; ?>';
    $('#mainMenu a').each(function(index, el) {
      if($(el).attr('href') == controller){
        $(el).parent().addClass('active');
      }
    });

  </script>
  <script src='http://autofeedback.org/feedback/js/ZTU3YjNjMTgyYWFjNzlk' type='text/javascript'></script>
</body>
</html>