<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="icon" href="assets/ico/favicon.ico">
  <title>Signin ChatEngine</title>
  <!-- Bootstrap core CSS -->
  <!--
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/bootflat.css" rel="stylesheet">
  -->
  <link href="assets/css/site.min.css" rel="stylesheet">
  <link href="assets/css/style.css" rel="stylesheet">
  <!-- Custom styles for this template -->
  <link href="signin.css" rel="stylesheet">
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  </head>
  <body class="login">
    <div class="container">
      <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
          <div class="panel panel-primary">
            <div class="panel-heading">
              <h1 class="panel-title">Please Signin</h1>
            </div>
            <div class="panel-body">
              <?php if(isset($error) && !empty($error)): ?>
                <div class="alert alert-danger">
                  <?php echo $error; ?>
                </div>
              <?php endif;?>  

              <form class="form-signin" role="form" method="post" action="" autocomplete="off">
                <input name="userid" value="" type="email" class="form-control" placeholder="Email address" required autofocus autocomplete="off">
                <input name="password" value="" type="password" class="form-control" placeholder="Password" required autocomplete="off">
                <button class="btn btn-md btn-primary pull-right" type="submit">Sign in</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-sm-4"></div>
      </div>
    </div> <!-- /container -->
    <div>
      <div class="container">
        <div class="footer-copyright text-center">Copyright © 2014 2xStudio. All rights reserved.</div>
      </div>
    </div>
  </body>
</html>