<?php defined('ROOT_DIR') or die('No direct access allowed');
/**
* configuration controller
* controller to handle all configuration process
*/
class ConfigurationController extends AuthController
{
  public function indexAction()
  {
    $view = new View('index');
    $view->set('title', 'Configuration');
    $view->render();
  }
}