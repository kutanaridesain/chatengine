<?php
/**
* 
*/
class AuthController extends Controller
{
  public function __construct()
  {
    if(!isset($_SESSION['auth'])){
      $this->redirect('user');
    }
  }
}