<?php
/**
* test controller
*/
class TestController extends Controller
{
  public function cobaAction()
  {
    $view = new View('coba');
    // $view = $this->loadView('coba');
    $parm = 'this is parm';
    $arrParams = array(
      'one' => 'satu',
      'two' => 'dua'
    );
    $view->set('parm', $parm);
    $view->set('arrParams', $arrParams);
    $view->render();
  }

  public function indexAction()
  {
    $modelCommen = new Comet_clientModel();
    $modelCommen->insert('ss');
    echo "This is index of home";
  }
}