<?php

/**
* 
*/
class DashboardController extends AuthController
{
  
  public function indexAction()
  {
    $view = new View('index');
    $view->set('title', 'Dashboard');
    $view->render();

  }
}