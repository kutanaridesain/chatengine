<?php
/**
* 
*/
class UserController extends Controller
{
  public function indexAction()
  {
    $error = null;
    if($this->isPost()){
      // $modelUser = $this->loadModel('user');
      $modelUser = new UserModel();
      if($modelUser->doLogin($_POST)){
        $_SESSION['auth'] = array(
          'userid'=>$_POST['userid']
        );
        $this->redirect('dashboard');
      }else{
        $error = 'Invalid login id or password!';
      }
    }

    $view = new View('index');
    $view->set('error', $error);
    $view->customLayout('login');
    $view->render();
  }

  public function logoutAction()
  {
    unset($_SESSION['auth']);
    $this->redirect('user');
  }
}