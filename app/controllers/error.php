<?php
defined('ROOT_DIR') or die('No direct script access.');
/**
* @author Fery Putra Tarigan
* @copyright 2014 2xstudio
* @version 0.1b
*/

class ErrorController extends Controller
{
  public function indexAction()
  {
    echo "page not found";
  }
}