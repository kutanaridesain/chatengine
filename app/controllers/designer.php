<?php defined('ROOT_DIR') or die('No direct script access');
/**
* designer controller
* handle all designer action 
*/
class DesignerController extends AuthController
{
  public function indexAction()
  {
    $view = new View('index');
    $view->set('title', 'Designer');
    $view->render();
  }
}