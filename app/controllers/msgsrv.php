<?php
/**
* 
*/
class MsgsrvController extends Controller
{
  /*public function indexAction()
  {
    // if ( $msgId == 1) {
    //   header('HTTP/1.0 404 Not Found');
    //   die();
    // }

    $messagesArray = array(
        'Raw denim you probably haven\'t heard of them jean shorts Austin.',
        'Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.',
        'Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.',
        'Well done! You successfully read this important alert message.',
        'Oh snap! Change a few things up and try submitting again.',
        'Warning! Best check yo self, you\'re not looking too good.',
        'Heads up! This alert needs your attention, but it\'s not super important.'
    );

    $usersArray = array(
        array('Bob Doe', 'Admin'),
        array('Jenifer', 'Admin'),
        array('Joe', 'Admin'),
        array('Rita', 'Admin'),
        array('Alexa', 'Admin'),
        array('John', 'Admin'),
        array('Jim', 'Admin')
    );

    $msgId = rand(0,6);
    $message = array();
    $message['id'] = $msgId;
    $message['nick'] = $usersArray[$msgId][rand(0,1)];
    $message['time'] = date('Y-m-d H:i:s');
    $message['content'] = $messagesArray[rand(0,6)];//'I have number ' . rand(2,10);

    sleep(rand(2,10));
    // header()
    echo json_encode($message);

  }*/
	
	public function indexAction(){
			
		$message = array("status"=>"0", "message"=>"Unknown error.");
		
		try{			
			$modelMessage = new MessageModel();
			$result = $modelMessage->selectAll();
				
			$message = array("status"=>"1", "message"=>$result);
		}
		catch (Exception $e)
		{
			$message = array("status"=>"0", "message"=>"System error.");
		}
		
		echo json_encode($message);
	}
	
	
	public function unreadAction(){
			
		$message = array("status"=>"0", "message"=>"Unknown error.");
		
		try{			
			$modelMessage = new MessageModel();
			$result = $modelMessage->selectUnread();
				
			$message = array("status"=>"1", "message"=>$result);
		}
		catch (Exception $e)
		{
			$message = array("status"=>"0", "message"=>"System error.");
		}
		
		echo json_encode($message);
	}
  
	public function insertAction()
	{
		$message = array("status"=>"0", "message"=>"Unknown error.");
		
		try{
			if($this->isPost()){
				$modelMessage = new MessageModel();
				$modelMessage->insert($_POST);
				
				$message = array("status"=>"1", "message"=>"Success.");
			}
		}
		catch (Exception $e)
		{
			$message = array("status"=>"0", "message"=>"System error.");
		}
		
		echo json_encode($message);
	
	}
	
	
	public function updateAction()
	{
		$message = array("status"=>"0", "message"=>"Unknown error.");
		
		try{
			if($this->isPost()){
				$modelMessage = new MessageModel();
				
				
				$modelMessage->updateStatus($_POST);
				
				$message = array("status"=>"1", "message"=>"Success.");
			}
		}
		catch (Exception $e)
		{
			$message = array("status"=>"0", "message"=>"System error.");
		}
		
		echo json_encode($message);
	
	}
}