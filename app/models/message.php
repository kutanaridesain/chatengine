<?php
/**
* 
*/
class MessageModel extends Model
{
	protected $_table = 'messages';
	
	public function selectAll()
	{
		$result = NULL;
		$query = sprintf('SELECT * FROM `%s` ', $this->_table);
		$result = $this->query($query);
		return $result;
	}
	
	public function selectUnread()
	{
		$result = NULL;
		$query = sprintf('SELECT * FROM `%s` WHERE `is_read`= 0 ', $this->_table);
		$result = $this->query($query);
		return $result;
	}

	public function insert($fields)
	{
		$isread = 0;
		//if($this->_escape($fields['from'])==1){
		//	$isread = 1;
		//}

		$sqlString = sprintf('INSERT INTO `%s` (`message`, `from`, `insert_datetime`, `to`, `is_read`, `id_client_side`) VALUES ("%s", "%s", "%s", "%s", "%s", "%s")', 
			$this->_table, 
			$this->_escape($fields['message']), 
			$this->_escape($fields['from']), 
			date('Y-m-d H:i:s'), 
			$this->_escape($fields['to']), 
			$isread,
			$this->_escape($fields['clientid'])
		 );
		 
		//echo $sqlString;exit();
		parent::execute($sqlString);
	}
	
	public function updateStatus($fields)
	{
		$result = NULL;
		
		$ids = $this->_escape($fields['ids']);
		$arrIds = explode(",",$ids);
		
		//print_r($arrIds);die();
		
		for ($x=0; $x<count($arrIds); $x++) {
			$query = sprintf('UPDATE `%s` SET `is_read`= 1 WHERE `id_client_side`= \''. $arrIds[$x].'\'' , $this->_table);
			
			$result = $this->execute($query);
		}
		return $result;
	}
}

