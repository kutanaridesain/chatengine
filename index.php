<?php
/**
* @author Fery Putra Tarigan
* @link kutanari@yahoo.com kutanari
* @copyright 2014 2xStudio
* @version 0.1b
*/

define('ROOT_DIR', realpath(dirname(__FILE__)) .'/');
define('APP_DIR', ROOT_DIR .'app/');

require 'conf.php';
// var_dump($configs);
require 'libs/app.php';