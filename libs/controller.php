<?php
defined('ROOT_DIR') or die('No direct script access.');

/**
* @author Fery Putra Tarigan
* @copyright 2014 2xStudio
* @version 0.1b
*/


class Controller
{
  public function __construct()
  {
    spl_autoload_register('autoloadModel');
  }

  public function redirect($path)
  {
    header('Location: '. $GLOBALS['conf']['SITE']['baseURL'] . $path);
  }

  public function isPost()
  {
    $result = FALSE;
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
      $result = TRUE;
    }
    return $result;
  }

  public function loadModel($model)
  {
    include APP_DIR . 'models/' . $model . '.php';
    $model = ucfirst($model) . 'Model';
    return new $model;
  }


  // public function loadView($viewName)
  // {
  //   $view = new View($viewName);
  //   return $view;
  // }
}
