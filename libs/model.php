<?php

/**
* 
*/
class Model
{

  protected $_mysqli;
  protected $_connection;
  public function __construct()
  {
    $dbConfig = $GLOBALS['conf']['DB'];
    // $this->_mysqli = new mysqli($dbConfig['host'], $dbConfig['userName'], $dbConfig['password'], $dbConfig['dbName']);
    // if(mysqli_connect_errno()){
    //   exit(printf("Connection failed: %s\n", mysqli_connect_error()));
    // }

    $this->_connection = mysql_pconnect($dbConfig['host'], $dbConfig['userName'], $dbConfig['password']) or die('MySQL Error: '. mysql_error());
    mysql_select_db($dbConfig['dbName'], $this->_connection);
  } 

  protected function _escape($string)
  {
    // return $this->_mysqli->escape_string($string);
    return mysql_real_escape_string($string);
  }

  public function query($escapedStringQuery)
  {
    // $this->_mysqli->query($escapedStringQuery);
    $result = mysql_query($escapedStringQuery) or die('MySQL Error: '. mysql_error());
    $resultObjects = array();

    while($row = mysql_fetch_object($result)) $resultObjects[] = $row;

    return $resultObjects;
  }

  public function execute($query)
  {
    $exec = mysql_query($query) or die('MySQL Error: '. mysql_error());
    return $exec;
  }

  public function insert($escapedStringQuery)
  {
    $this->_mysqli->query($escapedStringQuery);
    return $this->_mysqli->affected_rows;
  }

}