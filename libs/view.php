<?php 

/**
* 
*/
class View
{
  private $_templatePath;
  private $_layoutFile = 'layout';
  private $_variablesCollections = array();
  
  public function __construct($templateName)
  {
    $this->_templatePath = APP_DIR . 'views/' . $GLOBALS['controllerName'] . '/' . $templateName . '.php';
    if(!is_file($this->_templatePath)){
      // throw new Exception("Error Processing Request, view file not found", 1);
      die('View file not found for ' . $GLOBALS['actionName'] . ' on ' . $GLOBALS['controllerName'] . 'Controller');
    }
  }

  public function set($variable, $value)
  {
    $this->_variablesCollections[$variable] = $value;
  }

  public function customLayout($layoutFile)
  {
    $this->_layoutFile = $layoutFile;
  }

  public function render()
  {
    $controller = $GLOBALS['controllerName'];
    extract($this->_variablesCollections, EXTR_OVERWRITE);

    ob_start();
    require($this->_templatePath);
    $content = ob_get_clean();

    $this->_checkLayout();
    ob_start();
    require(APP_DIR . 'views/' . $this->_layoutFile . '.php');
    echo ob_get_clean();
  }

  private function _checkLayout()
  {
    if(!is_file(APP_DIR . 'views/' . $this->_layoutFile . '.php') ){
      throw new Exception("Error Processing Request, layout file not found", 1);
    }
  }
}