<?php
defined('ROOT_DIR') or die('No direct script access.');
/**
* @author Fery Putra Tarigan
* @copyright 2014 2xstudio
* @version 0.1b
*/

if(!isset($_SESSION)) session_start();

function autoloadModel($model)
{
  $modelName = preg_replace('/Model/', '', $model);
  if($model != $modelName){
    $model = strtolower(str_replace('Model', '', $model));
    include APP_DIR . 'models/' . $model . '.php';
  }
}

function autoloadController($controller)
{
  $controllerName = preg_replace('/Controller/', '', $controller);
  if($controller != $controllerName){
    $controller = strtolower(str_replace('Controller', '', $controller));
    include APP_DIR . 'controllers/' . $controller . '.php';
  }
}
spl_autoload_register('autoloadController');

require ROOT_DIR . 'libs/view.php';
require ROOT_DIR . 'libs/controller.php';
require ROOT_DIR . 'libs/model.php';

$reqUrl = (isset($_SERVER['REQUEST_URI'])) ? $_SERVER['REQUEST_URI'] : '';
$scriptPath  = (isset($_SERVER['PHP_SELF'])) ? $_SERVER['PHP_SELF'] : '';

$url = '';
$params = array();
$keys = array();
$values = array();

if($reqUrl != $scriptPath){
  $url = trim(preg_replace('/'. str_replace('/', '\/', str_replace('index.php', '', $scriptPath)) .'/', '', $reqUrl, 1), '/');
}

$parameterStartingPoint = 2;
$uriComponent = explode('/', $url);

$controllerName = (isset($uriComponent[0]) && !empty($uriComponent[0])) ? $uriComponent[0] : $GLOBALS['conf']['SITE']['defaultController'] ;
$actionName = (isset($uriComponent[1]) && !empty($uriComponent[1])) ? $uriComponent[1] : 'index' ;
$controllerPath = APP_DIR . 'controllers/' . $controllerName . '.php';

if(is_dir(APP_DIR . 'controllers/' . $uriComponent[0])){
  $parameterStartingPoint = 3;

  $controllerName = (isset($uriComponent[1]) && !empty($uriComponent[1])) ? $uriComponent[1] : $GLOBALS['conf']['SITE']['defaultController'] ;
  $actionName = (isset($uriComponent[2]) && !empty($uriComponent[2])) ? $uriComponent[2] : 'index' ;
  $controllerPath = APP_DIR . 'controllers/' . $uriComponent[0] . '/' . $controllerName . '.php';
}
// if(!empty($url)){
//   for ($i=0; $i < count($uriComponent); $i++) { 
//     if($i >= 2){
//       if($i%2){
//         $values[] = $uriComponent[$i];
//       }else{
//         $keys[] = $uriComponent[$i];
//       }
//     }
//   }

//   $params = array_combine($keys, $values);
// }
$errorControllerPath = APP_DIR . 'controllers/error.php';

if(file_exists($controllerPath)){
  require_once($controllerPath);
}else{
  $controllerName = 'error';
  $actionName = 'index';
  require_once($errorControllerPath);
}

$controllerClassName = ucfirst($controllerName . 'Controller');
$actionName = $actionName . 'Action';
$obj = new $controllerClassName;
call_user_func_array(array($obj, $actionName), array_slice($uriComponent, $parameterStartingPoint));
// call_user_func_array(array($obj, $actionName), $params);
exit();