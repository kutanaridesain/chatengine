var jQ2x;
function parseMessage(type, msg) {
    var msgId = '#' + msg.id;
    if (jQ2x('#tabButton').find(msgId).size() == 0) {
        jQ2x('#tabButton').append('<li id="' + msg.id + '"><a href="#' + msg.id + 'x" data-toggle="tab">' + msg.nick + '</a></li>');
        jQ2x('#chatboxTabs').append('<div class="tab-pane fade" id="' + msg.id + 'x"></div>');
    }
    // jQ2x('#' + msg.id + 'x').append('<div class="pull-left panel panel-default ' + type + '"><div class="panel-heading"><h3 class="panel-title">' + msg.nick + '</h3></div><div class="panel-body">' + msg.content + '</div></div></div>');

    if (msg.nick == 'Admin') {
        jQ2x('#' + msg.id + 'x').append('<div class="msg-admin"><div class="itemdiv dialogdiv"><div class="user"><span class="avatar">Admin</span></div><div class="body"><div class="name"><a href="#">' + msg.nick + '</a></div><div class="time"><i class="icon-time"></i><span class="blue">' + msg.time + '</span></div><div class="text">' + msg.content + '</div></div></div></div>');
    } else {
        jQ2x('#' + msg.id + 'x').append('<div class="itemdiv dialogdiv"><div class="user"><span class="avatar">Guest</span></div><div class="body"><div class="time"><i class="icon-time"></i><span class="blue">' + msg.time + '</span></div><div class="name"><a href="#">' + msg.nick + '</a></div><div class="text">' + msg.content + '</div></div></div>');
    }
    console.log(jQ2x('#' + msg.id + 'x').height());
    jQ2x('#chatboxTabs').scrollTop(jQ2x('#' + msg.id + 'x').height());
    // jQ2x('#tabButton a:last').tab('show');
}

function poolingMessage() {
    jQ2x.ajax({
        url: 'http://localhost/commet/latihan/msgsrv/',
        type: 'GET',
        async: true,
        // cache: false,
        timeout: 50000,
        dataType: 'JSON',
    }).done(function (data) {
        parseMessage("new", data);
        setTimeout(poolingMessage, 1000);
        // console.log(data);
    }).fail(function (XMLHttpRequest, textStatus, errorThrown) {
        parseMessage("error", textStatus + " (" + errorThrown + ")");
        setTimeout(poolingMessage, 15000);
        // console.log(textStatus);
    }).always(function () {
        // console.log("complete");
    });
}
//jQ2x(document).ready(function () {
//    //   poolingMessage();
//    jQ2x('#chatboxTabs').slimScroll({
//        height: '350px'
//    });
//    // jQ2x('#tabButton').slimScroll({
//    //     height: '350px'
//    // });
//});



(function () {
    if (typeof (jQuery) !== "undefined") {
        jQ2x = $;
        poolingMessage();
    } else {
        var elmHead = document.getElementsByTagName('head')[0];
        var elmScript = document.createElement('script');
        //       elmScript.src = 'http://code.jquery.com/jquery-1.8.3.min.js';
        elmScript.src = 'http://code.jquery.com/jquery-1.9.1.min.js';
        var elmScript = elmHead.appendChild(elmScript);
        elmScript.onload = function () {
            alert(typeof (jQuery));
            jQ2x = jQuery.noConflict();
            poolingMessage();
        };
        //       alert(elmHead);
    }

})();